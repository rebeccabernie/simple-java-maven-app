package com.mycompany.app;

/**
 * Hello world!
 */
public class App
{
	// test comment to see if jenkins will build...
	// test2....
    private final String message = "Hello jenkinsssssss!";

    public App() {}

    public static void main(String[] args) {
        System.out.println(new App().getMessage());
	//System.out.println("Hello Jenkins");
    }

    private final String getMessage() {
        return message;
    }

}
